require 'nokogiri'

class Python3SubmissionTestRunner
  LANGUAGE_NAME = 'Python3'
  TEST_TIMEOUT_DURATION = '2m'

  attr_reader :test_suits

  # test if the content type for the test file is allowed return the error message to show if not allowed
  def self.test_content_type_is_allowed(content_type)
    # 'Dateityp muss jar sein' unless content_type == 'application/x-java-archive'
    # raise NotImplementedError
  end

  # test if the content type for the submission file is allowed return the error message to show if not allowed
  def self.submission_content_type_is_allowed(content_type)
    # 'Dateityp muss zip sein' unless content_type == 'application/zip'
    # raise NotImplementedError
  end

  def prepare_test(submission_test)
    @dir = Dir.mktmpdir
    @path = Pathname.new @dir
    @run_dir_path = @path.join 'run_dir'
    @run_dir = Dir.mkdir @run_dir_path.to_s

    unzip submission_test.submission.current_path, @path

    @submissions = find_submissions @path
    FileUtils.copy submission_test.test.current_path, @run_dir_path.to_s
    @test_suits = ['nosetests']
  end

  def clean_after_test(submission_test)
    FileUtils.remove_entry_secure @dir
  end

  def test_cases(submission_test, test_suit)
    `cd "#{@run_dir_path.to_s}"; python3 -m nose --with-xunit --xunit-file __out.xml --collect-only`
    doc = File.open(@run_dir_path.join('__out.xml').to_s) { |f| Nokogiri::XML(f) }
    test_case_elements = doc.xpath '//testcase'
    #TODO default amount of points
    test_cases_hash_array = test_case_elements.collect do |element|
      test_case_name = element.attr 'name'

      [
          test_case_name,
          {
              #TODO default amount of points
              points: 0
          }
      ]
    end
    Hash[test_cases_hash_array]
  end

  def test(submission_test, submission, test_suit)
    # the Name of the jar could be valid shell commands, so we rename it to prevent "Shell injection attacks"
    FileUtils.move @path.join(submission).to_s, @run_dir_path.join(submission).to_s
    run_test @run_dir_path.to_s
  end

  def submissions
    @submissions.map { |e| File.basename e }
  end

  private
  def run_test(run_dir)
    #TODO sudo slave
    `cd "#{run_dir}"; python3 -m nose --with-xunit --xunit-file __out.xml`
    doc = File.open(@run_dir_path.join('__out.xml').to_s) { |f| Nokogiri::XML(f) }
    test_case_elements = doc.xpath '//testcase'
    test_cases_hash_array = test_case_elements.collect do |element|
      test_case_name = element.attr 'name'
      failure = element.at_xpath './/failure'

      unless failure.nil?
        failure_message = failure.attr 'message'
        failure_type = failure.attr 'type'
        failure_trace = failure.content
      end

      [
          test_case_name,
          {
              #TODO default amount of points
              'points' => 0,
              'successful' => failure.nil?,
              'failure_message' => failure_message,
              'failure_exception' => failure_type,
              'failure_trace' => failure_trace,
              'extras' => {}
          }
      ]
    end
    Hash[test_cases_hash_array]
  end

  def unzip(file, dest_dir)
    Zip::File.open(file) do |zip_file|
      zip_file.each do |entry|
        output_path = dest_dir.join entry.name
        FileUtils.mkdir_p output_path.dirname
        zip_file.extract entry, output_path unless File.exist? output_path
      end
    end
  end

  def find_submissions(path)
    Dir[path.join('*.py').to_s]
  end
end