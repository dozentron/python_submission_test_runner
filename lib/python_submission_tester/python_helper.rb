# coding: utf-8
require 'zip'
require 'open3'
require 'mimemagic'

module PythonSubmissionTester
  module PythonHelper
    LIB_DIR =  Pathname.new(File.dirname(__FILE__)).join("../python")
    LANGUAGE_NAME = 'Python3'
    MOSS_LANGUAGE = 'python'
    FIND_TESTSUITE_SCRIPT = LIB_DIR.join("find_test_suites.py")
    FIND_TESTCASES_SCRIPT = LIB_DIR.join("find_test_cases.py")
    RUN_SUITE_SCRIPT = LIB_DIR.join("run_suite.py")
    TEST_TIMEOUT_DURATION = '2m'

    def student_help_path
      '/doc#_python'
    end
    def teacher_help_path
      '/doc#_python'
    end
    def moss_language
      MOSS_LANGUAGE
    end
    # calls block with all source file contents that may be relevant to moss
    def source_files(carrierwave_uploader)
      run_unzipped carrierwave_uploader do |path|
        path_name = Pathname.new path

        source_files = Dir[path_name + '**/*.py']

        source_files.each do |source_file|
          yield File.basename(source_file), File.open(source_file)
        end
      end
    end

    def test_suit_names_for(testZips, stub)
      run_unzipped *testZips, stub do |path|
        cmd = "#{PythonSubmissionTester.python3_path} #{FIND_TESTSUITE_SCRIPT.to_s} \"#{path}\""
        env = {"PYTHONPATH" => "#{LIB_DIR}:#{path}"}
        stdout,stderr,status = Open3.capture3(env, cmd)
        if status.exitstatus == 0
          return stdout.lines.map { |l| l.strip }
        else
          raise "Tests konnten nicht geladen werden:\n#{stderr}"
        end
      end
    end
    def test_cases_for_testsuit(testZips, stub, test_suit_name)
      run_unzipped *testZips, stub do |path|
        cmd = "#{PythonSubmissionTester.python3_path} #{FIND_TESTCASES_SCRIPT.to_s} \"#{test_suit_name}\""
        env = {"PYTHONPATH" => "#{LIB_DIR}:#{path}"}
        stdout,stderr,status = Open3.capture3(env, cmd, chdir: path.to_s)
        if status.exitstatus == 0
          return JSON.parse stdout
        else
          raise "Test cases wurden nicht gefunden:\n#{stderr}"
        end
      end
    end

    def validate_submission_structure(submission_file, test_file)
      msg = validate_is_zip? submission_file
      return {error: msg} unless msg.nil?
      msg = validate_contains_py_files? submission_file
      return {error: msg} unless msg.nil?
    end
    def validate_test_structure(test_file, stub_file)
      msg = validate_is_zip? test_file
      return {error: msg} unless msg.nil?
      msg = validate_contains_py_files? test_file
      return {error: msg} unless msg.nil?
    end

    def validate_is_zip?(uploader)
      err_msg = "Datei muss ein ZIP-Archiv sein"
      mime_type_path = MimeMagic.by_path(uploader.current_path)
      mime_type_magic = MimeMagic.by_magic(uploader.file.read)

      if mime_type_path.nil? && mime_type_magic.nil?
        return err_msg
      end

      begin
        Zip::File.open(uploader.current_path) {}
      rescue
        return err_msg
      end
    end

    def validate_contains_py_files?(uploader)
      err_msg = "Archiv muss '.py' Dateien enthalten"
      run_unzipped uploader do |path|
        path_name = Pathname.new path
        return err_msg if Dir[path_name+'**/*.py'].empty?
      end
    end


    # Setup bubblewrap for python:
    # . #content of test & submissions
    # ├── examples #submission content
    # │   ├── __init__.py
    # │   ├── test_calculator.py
    # │   └── test_number.py
    # └── libs #our files for running tests
    #      ├── find_test_cases.py
    #      ├── find_test_suites.py
    #      ├── __pycache__
    #      │ └── utils.cpython-36.pyc
    #      ├── run_suite.py
    #      └── utils.py
    def setup_bubblewrap(cmd, rw_files)
      bwrap_cwd = "/tmp"
      runtime_deps = PythonSubmissionTester.bubblewrap_dependencies.map { |e| "--ro-bind #{e} #{e}" }.join(' ')
      rw_files_bind = rw_files.map { |s| "--bind #{s} #{bwrap_cwd}/#{File.basename(s)}" }.join(' ')

      %Q{timeout #{TEST_TIMEOUT_DURATION}
      bwrap
        #{runtime_deps}
        --dir #{bwrap_cwd}
        --dir /var
        --symlink ../tmp var/tmp
        --proc /proc
        --dev-bind /dev/urandom /dev/urandom
        --ro-bind #{PythonSubmissionTester.python3_path} #{PythonSubmissionTester.python3_path}
        --chdir #{bwrap_cwd}
        --unshare-all
        --ro-bind #{LIB_DIR} /tmp/libs
        --setenv PYTHONPATH /tmp/libs:/tmp
        #{rw_files_bind}
        #{cmd}
      }.gsub(/\s+/, ' ')
    end

    #copied from java-runner
    def run_unzipped(*carrierwave_uploaders)
      if block_given?
        Dir.mktmpdir do |tmp_dir|
          carrierwave_uploaders.each do |carrierwave_uploader|
            unzip(carrierwave_uploader.current_path, tmp_dir)
          end

          yield tmp_dir
        end
      else
        raise 'run_unzipped needs a block to run!'
      end
    end
    #copied from java-runner
    def unzip(file, dest_dir)
      dest_dir = Pathname.new dest_dir
      Zip::File.open(file) do |zip_file|
        zip_file.each do |entry|
          output_path = dest_dir.join entry.name.strip
          FileUtils.mkdir_p output_path.dirname
          zip_file.extract(entry, output_path) {true}
        end
      end
    end
  end
end
