# coding: utf-8
require 'zip'
require 'open3'
require 'mimemagic'
require 'python_submission_tester/python_helper'

module PythonSubmissionTester
  class PythonSubmissionTestRunner
    include PythonSubmissionTester::PythonHelper

    def test(task, submission, test_suit)
      run_unzipped submission.submission, *task.test_jar_list do |working_dir|
        run_test = "#{PythonSubmissionTester.python3_path} ./libs/#{File.basename(RUN_SUITE_SCRIPT).to_s} \"#{test_suit}\""

        cmd = setup_bubblewrap(run_test, Dir[working_dir+"/**"])
        stdout,stderr,status = Open3.capture3(cmd)
        if status.exitstatus == 0
          return JSON.parse stdout
        else
          raise "Beim ausführen der Tests ist ein Fehler aufgetreten:\n#{stderr}"
        end
      end
    end
  end
end
