require "python_submission_tester/engine"

module PythonSubmissionTester
  # Your code goes here...
  mattr_accessor :bubblewrap_dependencies
  mattr_accessor :python3_path

  self.python3_path = '/usr/bin/python3'

  self.bubblewrap_dependencies = [
    "/usr",
    "/lib",
    "/lib64",
    "/sbin"
  ]
end
