import unittest as utest
import json
import sys
import utils


"Runs all given tests and returns a dictionary containing the errors & failures."
def runTests(tests):
    json_dict = {}
    for case in tests:
        result = utest.TestResult()
        case.run(result)

        for [test_case, msg] in (result.failures+result.errors):
            #there is no simpler way to get the methodname of the  test
            method_name = test_case.id()
            #last line of message contains the Exception that where thrown: > Exception: hui that's an error <
            exception_type = msg.splitlines()[-1].split(":")[0]
            json_dict[method_name] = {
            "failureTrace": msg,
            "failureMessage": exception_type,
            "failureException": exception_type,
            "successful": False
            }
    return json_dict


"Add each test to the dictionary if it's not already in there."
def addSuccessfullTests(test_dict, tests):
    for case in tests:
       method_name = case.id()
       if method_name not in test_dict:
           test_dict[method_name] = {
               "successful": True
           }

if len(sys.argv) != 2:
    raise "Expected 1 CLi argument, the TestSuite name"

test_suite = sys.argv[1]
tests = utest.defaultTestLoader.loadTestsFromName(test_suite)

json_dict = runTests(tests)
addSuccessfullTests(json_dict, tests)

print(json.dumps(json_dict))
