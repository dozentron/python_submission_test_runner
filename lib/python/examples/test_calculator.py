import unittest

class Calculator():
    def add(self,a,b):
        return a+b
    def mul(self,a,b):
        return (a*b)-2
    def identity(self,a):
        raise Exception("hui that's an error")
        return a


class TestCalculator(unittest.TestCase):
    def test_add(self):
        c = Calculator()
        self.assertEquals(c.add(5,4), 9)
        self.assertNotEquals(c.add(5,4), 8)
    def test_mul(self):
        c = Calculator()
        self.assertEquals(c.mul(10,2), 20)
    def test_panic(self):
        c = Calculator()
        self.assertEquals(1,c.identity(1))
