import unittest as utest

"Returns the suite and test methodname of a given unittest.TestCase"
def getSuiteAndMethodName(test_case):
    [suite_name,_,method_name] = test_case.id().rpartition('.')
    return [suite_name, method_name]
