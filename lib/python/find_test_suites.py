import unittest as utest
import json
import sys

def find_test_suites(directory):
    loader = utest.defaultTestLoader
    tests = loader.discover(directory)

    for sub in utest.TestCase.__subclasses__():
        class_name = sub.__module__+"."+sub.__name__
        if not class_name.startswith("unittest"):
            print(class_name)

find_test_suites(sys.argv[1] if (len(sys.argv) == 2) else ".")
