import unittest as utest
import json
import sys
import utils

def find_test_cases(test_suite):
    tests = utest.defaultTestLoader.loadTestsFromName(test_suite)
    json_dict = {}
    for case in tests:
        method_name = case.id()
        json_dict[method_name] = {"extras": {}}

    return json_dict



if len(sys.argv) != 2:
    raise "Expected a test_suite_name as argument"
else:
    print(json.dumps(find_test_cases(sys.argv[1])))
