$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "python_submission_tester/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "python_submission_tester"
  s.version     = PythonSubmissionTester::VERSION
  s.authors     = ["Dominic Althaus"]
  s.email       = ["althaus.dominic@gmail.com"]
  s.homepage    = "http://magrathea.mni.thm.de/"
  s.summary     = "TestRunner for pyhton SubmissionTests"
  s.description = "TestRunner for pyhton SubmissionTests"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", ">= 5.0.0.rc1", "< 5.1"

  s.add_development_dependency "sqlite3"
end
